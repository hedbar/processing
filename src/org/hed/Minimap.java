package org.hed;

import processing.core.PApplet;

import java.util.*;

public class Minimap extends PApplet {


    public static void main(String[] args) {
        PApplet.main("org.hed.Minimap");
    }


    World world = new World(500, 600, this);

    int STATE_IDLE = 1, STATE_DRAGGED = 2, STATE_REARRANGING = 3;

    public void settings() {
        mySetSize();
    }

    public void setup() {
        mySetSize();
        frameRate(30);


        int testScenario = 0;

        if (testScenario > 0) {
            generateTest(1);
        } else {
            int num_of_rectangles = 6;
            for (int i = 0; i < num_of_rectangles; i++) {
                generateRandomRect();
            }
        }
    }


    private void generateTest(int i) {

        if (i == 1) {
            new Rect(308, 0, 192, 191, world);
            new Rect(146, 75, 166, 102, world);
            new Rect(0, 0, 146, 157, world);
        }

    }

    private Rect generateRandomRect() {
        int x = (int) (Math.random() * 400);
        int y = (int) (Math.random() * 400);
        int w = (int) (Math.random() * 100) + 100;
        int h = (int) (Math.random() * 100) + 100;
        Rect ret = new Rect(x, y, w, h, world);
        return ret;
    }


    public void mySetSize() {
        size(world.w, world.h);
    }

    public void draw() {
        background(0);
        world.draw();
    }

    public void mousePressed() {
        world.perform("mousePressed");
    }

    public void mouseDragged() {
        world.perform("mouseDragged");
    }

    public void mouseReleased() {
        world.perform("mouseReleased");
    }


    void myLog(String s) {
        //println(s);
    }

    class Tuple {
        private Object o1;
        private Object o2;
        private Object o3;

        Tuple(Object o1, Object o2) {
            this.o1 = o1;
            this.o2 = o2;
        }


        Tuple(Object o1, Object o2, Object o3) {
            this.o1 = o1;
            this.o2 = o2;
            this.o3 = o3;
        }
    }

    class World {
        private PApplet container;
        private int w, h;
        ArrayList<Rect> rects = new ArrayList<Rect>();
        ArrayList<Integer> sectionPoints = new ArrayList<Integer>();
        int stateNow = STATE_IDLE;
        Rect last_dragged = null;


        World(int w, int h, PApplet container) {
            this.w = w;
            this.h = h;
            this.container = container;
        }

        void register(Rect o) {
            rects.add(o);
            o.addWorld(this);
        }

        void setDragged(Rect o) {
            if (o == null) {
                stateNow = STATE_REARRANGING;
                resetExpansionAreas();
            } else {
                stateNow = STATE_DRAGGED;
                last_dragged = o;
            }
        }

        void perform(String action) {
            for (Rect l : rects) {
                switch (action) {
                    case "mouseReleased":
                        l.mouseReleased();
                        break;
                    case "mouseDragged":
                        l.mouseDragged();
                        break;
                    case "mousePressed":
                        l.mousePressed();
                        break;
                    default:
                        ;
                }
            }
        }

        public void draw() {
            if (stateNow == STATE_REARRANGING) {

                boolean was_there_a_move_in_the_current_round = false;

                for (Rect l1 : rects) {
                    boolean is_moved = ((Rect) l1).draw();
                    if (is_moved) {
                        was_there_a_move_in_the_current_round |= is_moved;

                    }
                }

                if (!was_there_a_move_in_the_current_round) {
                    stateNow = STATE_IDLE;
                    myLog("Finished Moving");
                    defineExpansion();
                    alignRectToZone();
                }


            } else {

                for (Integer p : sectionPoints) {
//                    line(0,p,width,p);
                }

                for (Rect l1 : rects) {
                    l1.draw();
                }
            }
        }

        private void resetExpansionAreas() {
            for (Rect r : rects) {
                r.lowerOuterBounry = r.upperOuterBounry = r.leftOuterBoundery = r.rightOuterBoundery = -1;
            }
        }


        private void alignRectToZone() {

            ArrayList<Tuple> arr = new ArrayList<Tuple>();
            for (Rect r : rects) {
                r.targetX = (r.rightOuterBoundery + r.leftOuterBoundery) / 2 - r.w / 2;
                r.targetY = (r.lowerOuterBounry + r.upperOuterBounry) / 2 - r.h / 2;
            }
        }


        private void defineExpansion() {
            final int section_start = 0, section_end = 1;

            sectionPoints = new ArrayList<Integer>();
            ArrayList<Tuple> arr = new ArrayList<Tuple>();
            for (Rect r : rects) {
                arr.add(new Tuple(r.y, section_start, r));
                arr.add(new Tuple(r.y + r.h, section_end, r));
            }

            Collections.sort(arr, new Comparator<Tuple>() {
                public int compare(Tuple o1, Tuple o2) {
                    return ((int) o1.o1) - ((int) o2.o1);
                }
            });
            int number_of_rect_in_this_y = 0;
            ArrayList<Rect> rect_in_zone = new ArrayList<Rect>();
            HashMap<Rect, ArrayList<Rect>> interactions = new HashMap<Rect, ArrayList<Rect>>();

            for (Tuple t : arr) {
                if (((int) t.o2) == section_start) {
                    number_of_rect_in_this_y += 1;
                    interactions.put((Rect) t.o3, new ArrayList<Rect>());
                    for (Rect r : rect_in_zone) {
                        interactions.get(r).add((Rect) t.o3);
                        interactions.get((Rect) t.o3).add(r);
                    }
                    rect_in_zone.add((Rect) t.o3);
                } else {
                    number_of_rect_in_this_y -= 1;
                    rect_in_zone.remove(t.o3);
                    setPossibleHoritontalBoundries((Rect) t.o3, interactions.get((Rect) t.o3));
                }
                if (number_of_rect_in_this_y == 0) {
                    sectionPoints.add((Integer) t.o1);
                }
            }

            // Switch to horizontal expansion
            arr = new ArrayList<Tuple>();
            for (Rect r : rects) {
                arr.add(new Tuple(r.leftOuterBoundery, section_start, r));
                arr.add(new Tuple(r.rightOuterBoundery, section_end, r));
            }
            Collections.sort(arr, new Comparator<Tuple>() {
                public int compare(Tuple o1, Tuple o2) {
                    return ((int) o1.o1) - ((int) o2.o1);
                }
            });
            int number_of_rect_in_this_x = 0;
            rect_in_zone = new ArrayList<Rect>();
            interactions = new HashMap<Rect, ArrayList<Rect>>();

            for (Tuple t : arr) {
                if (((int) t.o2) == section_start) {
                    number_of_rect_in_this_x += 1;
                    interactions.put((Rect) t.o3, new ArrayList<Rect>());
                    for (Rect r : rect_in_zone) {
                        interactions.get(r).add((Rect) t.o3);
                        interactions.get((Rect) t.o3).add(r);
                    }
                    rect_in_zone.add((Rect) t.o3);
                } else {
                    number_of_rect_in_this_y -= 1;
                    rect_in_zone.remove(t.o3);
                    setPossibleVerticalBoundries((Rect) t.o3, interactions.get((Rect) t.o3));
                }
            }

        }

        private void setPossibleHoritontalBoundries(Rect r, ArrayList<Rect> rect_in_zone) {
            int leftBoundry = 0, rightBoundry = w;
            for (Rect o : rect_in_zone) {
                if (r.x > o.x) {
                    leftBoundry = Math.max(leftBoundry, Math.max(o.x + o.w, o.rightOuterBoundery) + 1);
                } else {
                    if (o.leftOuterBoundery != -1) {
                        rightBoundry = Math.min(rightBoundry, Math.min(o.x, o.leftOuterBoundery) - 1);
                    } else {
                        rightBoundry = Math.min(rightBoundry, o.x - 1);
                    }
                }
            }

            r.leftOuterBoundery = leftBoundry;
            r.rightOuterBoundery = rightBoundry;

        }

        private void setPossibleVerticalBoundries(Rect r, ArrayList<Rect> rect_in_zone) {
            int upperBoundry = 0, lowerBoundry = h;
            if (rect_in_zone != null) {
                for (Rect o : rect_in_zone) {
                    if (r.y > o.y) {
                        upperBoundry = Math.max(upperBoundry, Math.max(o.y + o.h, o.lowerOuterBounry));
                    } else {
                        if (o.upperOuterBounry != -1) {
                            lowerBoundry = Math.min(lowerBoundry, Math.min(o.y, o.upperOuterBounry));
                        } else {
                            lowerBoundry = Math.min(lowerBoundry, o.y);
                        }
                    }
                }
            }

            r.lowerOuterBounry = lowerBoundry;
            r.upperOuterBounry = upperBoundry;

        }

        float[] calc_Shortest_way_out(Rect o1, Rect o2) {
            float[] overlap = new float[2];

            overlap[0] = Math.max(0, ((o1.w + o2.w) / 2) - Math.abs(o1.x + o1.w / 2 - (o2.x + o2.w / 2))) + 1;
            overlap[1] = Math.max(0, ((o1.h + o2.h) / 2) - Math.abs(o1.y + o1.h / 2 - (o2.y + o2.h / 2))) + 1;

            return overlap;
        }

        public boolean verify_move_and_adjust_inBoundingBox(int x_candidate, int y_candidate, Rect rect) {
            x_candidate = Math.max(x_candidate, 0);
            y_candidate = Math.max(y_candidate, 0);
            if ((x_candidate + rect.w) > this.w) {
                x_candidate = this.w - rect.w;
            }
            if ((y_candidate + rect.h) > this.h) {
                resize(y_candidate, rect);


            }
            boolean is_moved = (rect.x != x_candidate || rect.y != y_candidate);
            rect.x = x_candidate;
            rect.y = y_candidate;
            return is_moved;
        }

        int TOP_HEIGHT = 600;
        private void resize(int y_candidate, Rect rect) {
            int targetH = y_candidate + rect.h;
            if (targetH < TOP_HEIGHT) {
                this.h = (int) (targetH);
                surface.setSize(w, h);
            } else {
                float shrinkFactor = (float)TOP_HEIGHT / targetH;
                h = TOP_HEIGHT;
                w = (int) (w * shrinkFactor);
                for (Rect r : rects) {
                    r.worldShrink(shrinkFactor);
                }
                surface.setSize(w, h);

                stateNow = STATE_REARRANGING;
                resetExpansionAreas();
            }
        }
//                Drawing2D.prototype.size = function(aWidth, aHeight, aMode) { - from processing.js
//                container.setSize(w,h); // does nothing
//                container.size(w,h);  //size() can only be used inside settings().
    }

    static int baseRectID = 1;

    class Rect {

        int id;
        int x, y, w, h, xOffset, yOffset;
        int leftOuterBoundery = -1, rightOuterBoundery = -1, lowerOuterBounry = -1, upperOuterBounry = -1;
        boolean overBox = false;
        boolean locked = false;

        ArrayList<Rect> neighbours;
        World world;
        private float r, g, b;
        public int targetX = -1, targetY = -1;

        Rect(int x_, int y_, int w_, int h_, World d) {
            id = baseRectID++;
            this.x = x_;
            this.y = y_;
            this.w = w_;
            this.h = h_;
            d.register(this);
            setColor((float) Math.random() * 255, (float) Math.random() * 255, (float) Math.random() * 255);
        }

        public String toString() {
            return String.format("Rect %d -  x:%d, y:%d, w:%d, h:%d", id, x, y, w, h);
        }

        void addWorld(World world) {
            this.world = world;
            this.neighbours = world.rects;
        }

        boolean draw() {

            overBox = (mouseX > x && mouseX < x + w &&
                    mouseY > y && mouseY < y + h);


            boolean is_moved = false;
            if (world.stateNow == STATE_REARRANGING) {
                for (Rect o : neighbours) {
                    if (o != this) {
                        int x_orig = this.x, y_orig = this.y;
                        if (!(x + w < o.x || o.x + o.w < x || y + h < o.y || o.y + o.h < y)) {
                            float[] pressures = world.calc_Shortest_way_out(this, o);

                            int x_candidate = x + (int) (Math.signum(x - o.x) * Math.ceil(pressures[0] / 5)),
                                    y_candidate = y + (int) (Math.signum(y - o.y) * Math.ceil(pressures[1] / 5));

                            if (pressures[0] < pressures[1]) {
                                is_moved = world.verify_move_and_adjust_inBoundingBox(x_candidate, y, this);
                                if (!is_moved) {
                                    is_moved = world.verify_move_and_adjust_inBoundingBox(x, y_candidate, this);
                                }
                            } else {
                                is_moved = world.verify_move_and_adjust_inBoundingBox(x, y_candidate, this);
                                if (!is_moved) {
                                    is_moved = world.verify_move_and_adjust_inBoundingBox(x_candidate, y, this);
                                }
                            }
                            if (is_moved) {
                                myLog(String.format("Moved Rect %d from x:%d, y:%d to - x:%d, y:%d", this.id, x_orig, y_orig, this.x, this.y));
                                myLog("   Because of :" + o.toString());

                                break;
                            }
                        }
                    }
                }
            }


            stroke(200);
            if (leftOuterBoundery != -1) {
                fill(r, g, b, 200);
                rect(leftOuterBoundery, upperOuterBounry, rightOuterBoundery - leftOuterBoundery, lowerOuterBounry - upperOuterBounry);
                noStroke();
            }

            if (overBox) {
                stroke(255);
            }
            fill(r, g, b, 255);

            if (world.stateNow == STATE_IDLE) {
                if (targetX != -1 && (x != targetX || y != targetY)) {
                    x = (int) lerp((float) x, (float) targetX, 0.2f);
                    y = (int) lerp((float) y, (float) targetY, 0.2f);
                }
            }

            rect(x, y, w, h);

            return is_moved;
        }


        void mousePressed() {
            if (overBox) {
                locked = true;
                world.setDragged(this);
                fill(255, 255, 255);
                lowerOuterBounry = upperOuterBounry = leftOuterBoundery = rightOuterBoundery = -1;
                myLog("Clicked: " + this);
            }

            xOffset = mouseX - x;
            yOffset = mouseY - y;
        }


        void mouseDragged() {
            if (locked) {
                int x_candidate = mouseX - xOffset;
                int y_candidate = mouseY - yOffset;
                world.verify_move_and_adjust_inBoundingBox(x_candidate, y_candidate, this);
            }
        }

        void mouseReleased() {
            locked = false;
            world.setDragged(null);
        }


        public void setColor(float r, float g, float b) {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public void worldShrink(float shrinkFactor) {
            x *= shrinkFactor;
            y *= shrinkFactor;
            w *= shrinkFactor;
            h *= shrinkFactor;
            leftOuterBoundery *= shrinkFactor;
            rightOuterBoundery *= shrinkFactor;
            lowerOuterBounry *= shrinkFactor;
            upperOuterBounry *= shrinkFactor;
        }
    }


}