package org.hed;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by hedbn on 18-Nov-17.
 */
public class MinimapTests {
    @Test
    public void testPressureCalc() {

        Minimap m = new Minimap();
        Minimap.World world = m.new World(500,500, null);

        Minimap.Rect r1 = m.new Rect(0, 0, 100, 20,world);
        Minimap.Rect r2 = m.new Rect(20, 10, 60, 60,world);

        float[] overlaps = world.calc_Shortest_way_out(r1,r2);
        System.out.println(overlaps[0]);
        System.out.println(overlaps[1]);
        assertEquals(80, overlaps[0]);
        assertEquals(10, overlaps[1]);


        r1 = m.new Rect(0, 0, 100, 100,world);
        r2 = m.new Rect(60, 80, 100, 10,world);

        overlaps = world.calc_Shortest_way_out(r1,r2);
        System.out.println(overlaps[0]);
        System.out.println(overlaps[1]);
        assertEquals(40, overlaps[0]);
        assertEquals(20, overlaps[1]);


        r1 = m.new Rect(0, 0, 100, 100,world);
        r2 = m.new Rect(10, 10, 10, 10,world);

        overlaps = world.calc_Shortest_way_out(r1,r2);
        System.out.println(overlaps[0]);
        System.out.println(overlaps[1]);
        assertEquals(20, overlaps[0]);
        assertEquals(20, overlaps[1]);
    }
}
